.data
var1: .space 4
var2: .space 4
.extern ext1 4
.extern ext2 4
.text
.globl main
main:
li $t0, 12
li $t1, 34
sw $t0, var1
sw $t1, var2
lw $gp, var1
sw $gp, ext2
lw $gp, var2
sw $gp, ext1
li $v0, 10
syscall