.data
var1: .word 0x10
var2: .word 0x20
var3: .word 0x30
var4: .word 0x40
first: .byte 'h'
last: .byte 'w'
.text
.globl main
main: addu $s0, $ra, $0
la $t0, var1
la $t1, var2
la $t2, var3
la $t3, var4
lw $t4, first
lw $t5, last
sw $t3, var1 
sw $t2, var2
sw $t1, var3
sw $t0, var4
li $v0, 10
syscall